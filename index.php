<?php

const PATH_ROOT = __DIR__ . DIRECTORY_SEPARATOR;
const DS = DIRECTORY_SEPARATOR;
const HOST = 'database';
const USER = 'root';
const PASS = '';
const NAME = 'toys-r-us';

$db = mysqli_connect(HOST,USER,PASS,NAME);

session_start();
require_once PATH_ROOT . 'util' . DIRECTORY_SEPARATOR . 'functions.php';
require_once PATH_ROOT . 'app' . DIRECTORY_SEPARATOR . 'routeur.php';

routeurStart();

mysqli_close($db);

