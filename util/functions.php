<?php

/**
 * Fonction query sécurisée de mysql automatique avec 1 seul paramètre
 *
 * @param string $query Mysql query
 * @param mysqli $db Mysql database connect
 * @param string $get Param to get in URL
 * @param string $type Type of param in URL
 * @return void
 */
function queryMysqlAuto(string $query, mysqli $db, string $get = '', string $type) : array {

    $arr = [];

    if ($stmt = mysqli_prepare($db, $query)) {
        if ($get !== '' ) {
            $getRequest = $_GET[$get];
        }

        if (mysqli_stmt_bind_param($stmt, $type, $getRequest)) {
            mysqli_stmt_execute($stmt);

            $result = mysqli_stmt_get_result($stmt);
            mysqli_stmt_close($stmt);

            if ($result) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $arr[] = $row;
                }
                return $arr;
            }
        }

    }
}




/** Fonction pour aller chercher toutes les marques à mettre dans le header et la liste
 *
 * @return array
 */
function getBrands() :array
{
    $q_prep = 'SELECT name, id FROM brands';
    global $db;

    if ($stmt = mysqli_prepare($db, $q_prep)) {

        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);
        mysqli_stmt_close($stmt);
        if ($result) {
            while ($row = mysqli_fetch_assoc($result))
                $arr_menu[] =  $row;
        return $arr_menu;
        }else{
            header('location: /error');
        }
    }
}

/**Fonction pour aller chercher le nombre de jouets par marques
 *
 * @param $brand_id
 * @return void
 */
function getBrandList($brand_id)
{
    global $db;
    $q_prep = 'select count(id) as toy_total from toys where brand_id= ?';

    if ($stmt = mysqli_prepare($db, $q_prep)) {

        if (mysqli_stmt_bind_param($stmt, 'i', $brand_id)) {
            mysqli_stmt_execute($stmt);
            $result = mysqli_stmt_get_result($stmt);
            mysqli_stmt_close($stmt);

            if ($result) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $arr_menu[] = $row;
                }
            }

            foreach ($arr_menu as $menu) {
                echo($menu['toy_total']);
            };
        }
    }
}

