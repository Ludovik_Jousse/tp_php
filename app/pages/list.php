<h1>Les jouets</h1>

<form method="GET" class="formbrand">
    <select name ='brand_id'>
        <option value="" disabled selected>Quelle marque ?</option>
        <?php foreach ($brandList as $brand): ?>
        <option value="<?= $brand['id'] ?>"><?= $brand['name'] ?></option>
        <?php endforeach; ?>
    </select>
    <select name = 'order'>
        <option value="" disabled selected>Ordre</option>
        <option value="1">Croissant</option>
        <option value="2">Décroissant</option>
    </select>
    <button type="submit">OK</button>
</form>

<div class="toys-container">
    <?php foreach ($arr_brands as $key):?>
        <div class="toy-content" >
            <a href="/product?jouet=<?= $key['id'] ?>" class="toy-image">
                <img src="<?= 'assets/img/'. $key['image']?>" alt="">
            </a>

            <a href="/product?jouet=<?= $key['id'] ?>" class="toy-description">
                <p class="toy-name"><?= $key['name'] ?></p>
                <p class="toy-price"><?= $key['price'] ?> €</p>
            </a>
        </div>

    <?php endforeach; ?>
    </div>

<div class="pagination">
    <a href="/list?page=1">1</a>
    <a href="/list?page=2">2</a>
    <a href="/list?page=3">3</a>
</div>
