<?php
foreach ($oneToy as $key):?>
    <div class="product-container">
        <h1><?= $key['toyname'] ?></h1>


        <div class="content-container">

            <div class="product-left-part">

                <img src="assets/img/<?= $key['image'] ?>" alt="">
                <span class="product-price"><?= $key['price'] ?> €</span>

                <form action="" method="POST">

                    <select name="store_id" value="Quel magasin ?">
                        <option value="" disabled selected>Quelle magasin ?</option>
                        <?php foreach ($arr_store as $store): ?>
                            <option value="<?= $store['id'] ?>"><?= $store['city'] ?></option>
                        <?php endforeach; ?>

                    </select>
                    <button type="submit">GO</button>

                </form>

                <?php foreach ($arr_stock as $stock): ?>
                    <p class="stock-count">Stock: <span class='bold'><?= $stock['quantity'] ?></span></p>
                <?php endforeach; ?>

            </div>

            <div class="product-right-part">

                <p class="product-brand">Marque: <span class="bold"><?= $key['brandname'] ?></span></p>
                <div class="product-description"><?= $key['description'] ?></div>

            </div>
        </div>

    </div>
<?php endforeach;
