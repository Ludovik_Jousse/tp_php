<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>Document</title>
</head>
<body>
<div class="container">
<header>
<div class='logo-container'>
    <a href="/" class="logo">
        <img src="assets/img/logo.png" alt="Logo toys-r-us">
    </a>
    </div>
    <nav>
        <ul>
            <li><a href="/list?page=1">Tous les jouets</a></li>
            <li>Par Marque &dtrif;
                <ul class='submenu'>
                    <?php foreach($brandList as $menu):?>
                        <li><a href="/list?brand_id=<?= $menu['id'] ?>">
                                <?= $menu['name'] ?>
                                <span>(<?= getBrandList($menu['id']) ?>)</span>
                            </a></li>
                    <?php endforeach; ?>
                </ul>
            </li>
        </ul>
    </nav>

</header>
<main>

