<?php

function listRender()
{
    $page = getPageNumber();
    $brandList = getBrands();
    $arr_brands = listGetAll();

    if ($page && $brandList && $arr_brands){
    require_once PATH_ROOT . 'app' . DS . 'pages' . DS . 'partials' . DS . 'header.php';
    require_once PATH_ROOT . 'app' . DS . 'pages' . DS . 'list.php';
    require_once PATH_ROOT . 'app' . DS . 'pages' . DS . 'partials' . DS . 'footer.php';
    }else{
        header('location: /error');
    }

}

;

function listGetAll()
{
    global $db;
    $arr_brands = [];
    $order = 'DESC';
    $page = 0;
    if(!empty($_GET['page'])) {
        $page = $_GET['page'];
        $page === 1 ? $page = 0: $page = ($page - 1) *4 ;
    };

    if (!empty($_GET['ordre'])) {
        if ($_GET['ordre'] === '1') {
            $order = 'ASC';
        }
    }

    if (!empty($_GET['brand_id'])) {
        $arr_brands = queryMysqlAuto('select t.name, price, image, id from toys as t where brand_id = ? 
        ORDER BY price ' . $order . ' LIMIT '.$page.',4 ;', $db, 'brand_id', 'i');

    } else {

        $arr_list = mysqli_query($db, 'SELECT t.name, price, image, id FROM toys AS t 
        ORDER BY price ' . $order . ' LIMIT '.$page.',4 ;');

        while ($toy = mysqli_fetch_assoc($arr_list)) {
            $arr_brands[] = $toy;
        }

    }

    return $arr_brands;
}

function getPageNumber(){



    if(isset($_GET['page'])){
        $pageNumber = $_GET['page'];
    }else{
        $pageNumber = 2;
    }

    return $pageNumber;
}