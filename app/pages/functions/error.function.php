<?php

function errorRender()
{
    $brandList = getBrands();
    require_once PATH_ROOT . 'app' . DS . 'pages' . DS . 'partials' . DS . 'header.php';
    require_once PATH_ROOT . 'app' . DS . 'pages' . DS . 'error.php';
    require_once PATH_ROOT . 'app' . DS . 'pages' . DS . 'partials' . DS . 'footer.php';
}
