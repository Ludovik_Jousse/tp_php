<?php

function productRender()
{
    $brandList = getBrands();
    $oneToy = getOneToy();
    $arr_store = getAllStore();
    $arr_stock = getStock();

    if ($oneToy && $arr_store && $arr_stock) {
        require_once PATH_ROOT . 'app' . DS . 'pages' . DS . 'partials' . DS . 'header.php';
        require_once PATH_ROOT . 'app' . DS . 'pages' . DS . 'product.php';
        require_once PATH_ROOT . 'app' . DS . 'pages' . DS . 'partials' . DS . 'footer.php';
    } else {
        header('location: /error');
    }

}

;


/**Fonction pour avoir un seul jouet
 * @return array
 */
function getOneToy(): array
{
    global $db;
    $oneToy = queryMysqlAuto('SELECT t.name as toyname, t.price, t.description, t.image, b.name as brandname 
                                    FROM toys as t inner join brands as b on t.brand_id = b.id 
                                    WHERE t.id = ? ', $db, 'jouet', 'i');


    return $oneToy;
}




/**Fonction pour avoir toute la table des magasins
 * @return array
 */
function getAllStore(): array
{
    global $db;

    $q = mysqli_query($db, 'Select * from stores');

    while ($row = mysqli_fetch_assoc($q)) {
        $arrStore[] = $row;
    }
    return $arrStore;
}




/**Fonction qui va chercher le stock en fonction du jouet et du magasin, s'il n'y a pas de magasin choisi,
 * retourne le stock complet
 * @return array
 */
function getStock(): array
{
    global $db;
    if (!empty($_POST['store_id'])) {

        $q_prep = 'select SUM(stock.quantity) as quantity from toys join stock on stock.toy_id=toys.id 
                   where toy_id =? and store_id =? group by stock.toy_id;';

        if ($stmt = mysqli_prepare($db, $q_prep)) {
            $toyid = $_GET['jouet'];
            $storeid = $_POST['store_id'];
            if (mysqli_stmt_bind_param($stmt, 'ii', $toyid, $storeid)) {
                mysqli_stmt_execute($stmt);

                $result = mysqli_stmt_get_result($stmt);
                mysqli_stmt_close($stmt);

                if ($result) {
                    while ($row = mysqli_fetch_assoc($result))
                        $arr[] = $row;
                };
            }

        }
    } else {
        $arr = queryMysqlAuto('select SUM(stock.quantity) as quantity from toys join stock on stock.toy_id=toys.id
                                     where toy_id = ? group by stock.toy_id;', $db, 'jouet', 'i');
    }

    return $arr;
}