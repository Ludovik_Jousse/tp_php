<?php

function landingRender(){
    $brandList = getBrands();
    $arr_top_three = getTopThree();

    if($brandList && $arr_top_three){
    require_once PATH_ROOT . 'app' . DS . 'pages' . DS . 'partials' . DS . 'header.php';
    require_once PATH_ROOT . 'app' . DS . 'pages' . DS . 'landing.php';
    require_once PATH_ROOT . 'app' . DS . 'pages' . DS . 'partials' . DS . 'footer.php';
    }else{
        header('location: /error');
    }

}

;


/**Fonction pour aller chercher le top 3 des ventes
 * @return array
 */
function getTopThree(): array
{
    global $db;
    $topThree = mysqli_query($db, 'select SUM(quantity) as total, s.toy_id,t.name, t.price,t.image from sales as s 
    inner join toys as t on s.toy_id=t.id 
    GROUP BY s.toy_id, t.price 
    ORDER BY total DESC, price DESC limit 3;');

    while ($row = mysqli_fetch_assoc($topThree)) {
        $array[] = $row;
    }

    return $array;
}

