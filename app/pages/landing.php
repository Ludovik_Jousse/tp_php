<h1>Top 3 des ventes</h1>

<div class="toys-container">
    <?php foreach ($arr_top_three as $key):?>
        <div class="toy-content">
            <a class="toy-image" href="/product?jouet=<?= $key['toy_id'] ?>">
                <img src="<?= 'assets/img/'. $key['image']?>" alt="">
            </a>

            <a class="toy-description" href="/product?jouet=<?= $key['toy_id'] ?>">
                <p class="toy-name"><?= $key['name'] ?></p>
                <p class="toy-price"><?= $key['price'] ?> €</p>
            </a>
        </div>

    <?php endforeach; ?>
</div>