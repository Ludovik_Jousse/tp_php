<?php

function routeurStart(){

    $route = $_SERVER['REDIRECT_URL'] ?? '/';

    switch ($route){
        case '/';
            require_once 'pages' .DS .'functions' . DS .'landing-function.php';
            landingRender();
            break;

        case '/list':
            require_once 'pages' .DS .'functions' . DS .'list.function.php';
            listRender();
            break;

        case '/product':
            require_once 'pages' .DS .'functions' . DS .'product.function.php';
            productRender();
            break;

        case "/error":
            require_once 'pages' .DS .'functions' . DS .'error.function.php';
            errorRender();
            break;

        default:
            http_response_code(404);
            echo 'toto';
            break;

    }
};
